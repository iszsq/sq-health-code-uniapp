
export default {
	get(key){
		let value = uni.getStorageSync(key);
		return value;
	},
	set(key, value){
		uni.setStorageSync(key, value);
	},
	remove(key){
		uni.removeStorageSync(key);
	},
};
