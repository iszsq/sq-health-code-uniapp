import storejs from '@/utils/storejs';

/** 本地存储时用户相关key **/
export const userKeys = {
    // token 值
    token: 'token',
    // token 信息
    tokenInfo: 'tokenInfo',
    // 用户信息
    userInfo: 'userInfo',
};


/**
 * 是否已登录
 * @return true / false
 */
export function hasLogin() {
    let token = storejs.get(userKeys.token);
    if (token) {
        return true;
    }
    return false;
}
