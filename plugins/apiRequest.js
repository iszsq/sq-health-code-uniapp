import {userKeys} from '@/utils/authUtils.js';
import storejs from '@/utils/storejs';

/**
 * 接口请求封装
 * 使用uView中集成的请求框架：luch-request
 */

// 全局配置
uni.$u.http.setConfig((config) => {
    /* config 为默认全局配置*/
	// 根域名
    config.baseURL = `http://127.0.0.1:9696`; 
	// 超时时间
	config.timeout = 15000;
	// 响应数据类型
	config.responseType = 'json';
    return config
});

/**
 * 请求拦截
 */
uni.$u.http.interceptors.request.use((config)=>{
	console.log("请求拦截，config：", config);
	// 可以对config进行配置
	
	// 添加token至请求头
	let token = storejs.get(userKeys.token);
	config.header['satoken'] = token;
	
	return config;
}, (e)=>{
	console.error('请求前异常 ', e);
	// 请求前异常
	let data = {
		code: 101,
		message: '请求异常',
	};
	return Promise.reject(data)
});


/**
 * 响应拦截
 * 对响应的接口进行判断，处理
 */
uni.$u.http.interceptors.response.use((response) => {
	console.log("响应拦截，响应内容：", response);
	// 接口返回数据
	const data = response.data;
	// 状态码不等于200，则接口业务异常
	if (data.code !== 200) {
		let message = data.message || "系统错误，请重试！";
		// 显示toast提示
		showErrorToast(message);
		
		// 业务异常，返回至catch
		return Promise.reject(data);
	} else {
		// 正常返回
		return data;
	}
},(response) => { 
	// 对响应错误做点什么 （statusCode !== 200）
	// http响应码异常时
	let errMsg = response.errMsg || "系统错误，请重试！";
	// 显示toast提示
	showErrorToast(errMsg);
	return Promise.reject({
		code: 100,
		message: errMsg
	});
});

/**
 * 显示错误信息提示
 */
function showErrorToast(title){
	uni.showToast({
		title: title,
		icon: 'none'
	});
}

export default uni.$u.http;
