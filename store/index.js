import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

import user from './modules/user.js';

/**
 * 全局状态管理
 */
const store = new Vuex.Store({
	// 模块
	modules: {
		user: user,
	},
});


export default store;
