import storejs from '@/utils/storejs';
import {userKeys, hasLogin} from '@/utils/authUtils.js';

const keys = userKeys;

// 用户状态
const state = {
    //token信息
    tokenInfo: storejs.get(keys.tokenInfo),
    //当前token字符串
    token: storejs.get(keys.token),
    //用户信息
    userInfo: storejs.get(keys.userInfo),
}


const getters = {

}

const mutations = {
    setTokenInfo(state, tokenInfo){
        let token = tokenInfo.value;
        state.tokenInfo = tokenInfo;
        state.token = token;

        // 保存到本地
        storejs.set(keys.token, token);
        storejs.set(keys.tokenInfo, tokenInfo);
    },
    setUserInfo(state, userInfo){
        state.userInfo = userInfo;
        storejs.set(keys.userInfo, userInfo);
    },
}

const actions = {
    /**
     * 登录
     */
    login(context, formData){
        
    },
    /**
     * 注销登录
     */
    logout(context){
       return new Promise((resolve, reject)=>{
		   logout(context);
		   resolve();
	   });
    },
    /**
     * 获取个人信息
     */
    getUserInfo(context){
        
    },
};

/**
 * 注销登录
 * @param context state上下文
 */
function logout(context) {
    // 清除缓存
    storejs.remove(keys.token);
    storejs.remove(keys.tokenInfo);
    storejs.remove(keys.userInfo);

    if (context) {
        context.state.token = null;
        context.state.tokenInfo = null;
        context.state.userInfo = null;
        context.state.orgInfo = null;
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
