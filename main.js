import Vue from 'vue'
import App from './App'
import store from './store'
// 引入uView组件
import './plugins/uView2.js';
// Api接口请求
import Api from '@/api/index.js';

Vue.config.productionTip = false
App.mpType = 'app'

//api接口
Vue.prototype.$api = Api;


const app = new Vue({
	store,
    ...App
})
app.$mount()
