import apiRequest from '@/plugins/apiRequest.js';

/**
 * 健康码接口
 */

// 该接口基础路径
let modulePath = '/sqApi/appApi/healthCode';

export default {
	/**
	 * 我的健康码
	 */
	myHealthCode(){
		let res = apiRequest.get(
			modulePath + '/myHealthCode', 
		);
		return res;
	},
	/**
	 * 健康码申领
	 */
	apply(data){
		let res = apiRequest.post(
			modulePath + '/apply', 
			data
		);
		return res;
	},
};
