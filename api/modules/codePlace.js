import apiRequest from '@/plugins/apiRequest.js';

/**
 *  公共场所扫码点
 */

// 该接口基础路径
let modulePath = '/sqApi/appApi/codePlace';

export default {
	/**
	 * 公共场所注册
	 */
	regPlace(data){
		// 返回一个promise对象
		let res = apiRequest.post(
			modulePath + '/regPlace', 
			data,
		);
		return res;
	},
	/**
	 * 当前已注册的场所列表
	 */
	myPlaceList(data){
		// 返回一个promise对象
		let res = apiRequest.post(
			modulePath + '/myPlaceList', 
			data,
		);
		return res;
	},
};
