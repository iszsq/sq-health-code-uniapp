import apiRequest from '@/plugins/apiRequest.js';

/**
 * 用户模块
 *  用户管理
 */

// 该接口基础路径
let modulePath = '/sqApi/appApi/user';

export default {
	/**
	 * 个人注册
	 */
	register(data){
		// 返回一个promise对象
		let res = apiRequest.post(
			modulePath + '/register', 
			data,
		);
		return res;
	},
};
