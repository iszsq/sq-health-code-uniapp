import apiRequest from '@/plugins/apiRequest.js';

/**
 * 行政区域数据 接口
 */

// 该接口基础路径
let modulePath = '/sqApi/sysArea';

export default {
	/**
	 * 行政区域列表
	 */
	list(data){
		let res = apiRequest.get(
			modulePath + '/list', 
			{
				params: data,
			}
		);
		return res;
	},
};
