// 用户相关
import user from './modules/user/user.js';
// 健康码
import healthCode from './modules/healthCode/index.js';
// 省市区数据
import sysArea from './modules/sys/sysArea.js';
// 公共场所点
import codePlace from './modules/codePlace.js';


/**
 * 接口请求管理
 */
const api = {
	user: user,
	healthCode: healthCode,
	sysArea: sysArea,
	codePlace: codePlace,
};

export default api;